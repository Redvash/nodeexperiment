const Sequelize = require('sequelize');
const sequelize = require('../config/database.config');

const db = {
    User: require('./user.model')(sequelize,Sequelize),
    Campaign: require('./campaign.model')(sequelize,Sequelize),
    Book: require('./book.model')(sequelize,Sequelize),
    Note: require('./note.model')(sequelize,Sequelize),
    PageUpdate: require('./pageUpdate.model')(sequelize,Sequelize)
};

Object.keys(db).forEach(modelName => {
    if(db[modelName].associate) {
        db[modelName].associate(db);
    }
});

sequelize.sync();

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;