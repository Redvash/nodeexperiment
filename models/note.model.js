module.exports = (sequelize, DataTypes) => {

    let Note = sequelize.define('Note', {
        note: {
            type: DataTypes.TEXT('long'),
            allowNull: false
        }
    });

    Note.associate = function (models) {

    };

    return Note;
};