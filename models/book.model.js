module.exports = (sequelize, DataTypes) => {

    let Book = sequelize.define('Book', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        category: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        }
    });

    Book.associate = function (models) {
        models.Book.belongsTo(models.User, {
            as: 'owner'
        });
        models.Book.hasMany(models.Note, {
            as: 'notes',
            foreignKey: 'bookId'
        });
    };

    return Book;
};