module.exports = (sequelize, DataTypes) => {

    let PageUpdate = sequelize.define('PageUpdate', {
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });

    PageUpdate.associate = function (models) {

    };

    return PageUpdate;
};