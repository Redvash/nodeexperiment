module.exports = (sequelize, DataTypes) => {

    let Campaign = sequelize.define('Campaign', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        publicDescription: {
            type: DataTypes.STRING
        },
        privateNotes: {
            type: DataTypes.STRING
        }
    });

    Campaign.associate = function (models) {
        models.Campaign.belongsTo(models.User, {
            as: 'owner',
            allowNull: false
        });
    };

    return Campaign;
};