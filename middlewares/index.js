exports.registerMiddleware = (app) => {

    //Only register middlewares that should run on every request
    app.use(require('./logger.middleware'));
    app.use(require('./headers.middleware'));
    app.use(require('./cors.middleware'))
};