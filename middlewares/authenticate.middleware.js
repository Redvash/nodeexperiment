const jwt = require('jsonwebtoken');
const appConfig = require('../config/app.config');
const database = require('../models');

module.exports = (req,res,next) => {
    let authenticationToken = req.get('authenticationToken');

    if(!authenticationToken) {
        res.status(401).send();
    }

    let decodedToken = jwt.verify(authenticationToken,appConfig.secretKey);
    database.User.findOne({
        where: {
            id: decodedToken.userId
        }
    })
        .then(user => {
            if(user === null) {
                res.status(401).send();
                return;
            }

            req.user = user;
            next();
        })
};