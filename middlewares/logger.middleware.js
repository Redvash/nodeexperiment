const morgan = require('morgan');

module.exports = morgan('[:date[web]] :method to \':url\' - Response: \':status\' in :response-time milliseconds and stuff');
