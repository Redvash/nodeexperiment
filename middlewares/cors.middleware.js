const appConfig = require('../config/app.config');

module.exports = (req,res,next) => {
    let origin = req.get('origin');

    if(appConfig.allowedOrigins.indexOf(origin) !== -1) {
        res.set('Access-Control-Allow-Origin', origin);
        next();
    }
    else {
        res.status(401).send();
    }
};