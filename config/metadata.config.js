
const metadataConstructor = () => {
    let metadata = {
        controllers: []
    };

    let registerController = (controllerOptions) => {
        metadata.controllers.push({
            name: controllerOptions.name,
            description: controllerOptions.description,
            url: controllerOptions.url,
            endpoints: []
        });
    };
    let registerEndpoint = (controllerName,endpointOptions) => {
        const selectedController = metadata.controllers.find(arrayController => arrayController.name === controllerName);

        if(selectedController) {
            selectedController.endpoints.push({
                method: endpointOptions.method,
                description: endpointOptions.description,
                url: endpointOptions.url
            });
        }
    };
    let getMetadataJSON = () => {
        return JSON.stringify(metadata);
    };

    return {
        registerController,
        registerEndpoint,
        getMetadataJSON
    }
};

module.exports = metadataConstructor();