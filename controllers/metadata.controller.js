const express = require('express');
const metadataConfig = require('../config/metadata.config');

module.exports = () => {
    let router = express.Router();

    router.get('', (req,res) => {
        const rootDirectory = __dirname.replace('controllers','');

        res.sendFile(rootDirectory + 'pages/metadata/metadata.page.html');
    });
    router.get('/metadata.controller.js', (req,res) => {
        const rootDirectory = __dirname.replace('controllers','');
        res.sendFile(rootDirectory + 'pages/metadata/metadata.controller.js');
    });
    router.get('/metadata.styles.css', (req,res) => {
        const rootDirectory = __dirname.replace('controllers','');
        res.sendFile(rootDirectory + 'pages/metadata/metadata.styles.css');
    });

    router.get('/info', (req,res) => {
        const content = 'window.metadataInfo = ' + metadataConfig.getMetadataJSON();

        res.send(content);
    });
    return router;
};