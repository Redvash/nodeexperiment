const express = require('express');
const router = express.Router();

//Controllers
const authenticationController = require('./authentication.controller');
const booksController = require('./books.controller');
const notesController = require('./notes.controller');
const campaignsController = require('./campaigns.controller');
const pageUpdateController = require('./pageUpdates.controller');

const metadataController = require('./metadata.controller');

exports.registerControllers = (app,database) => {

    app.get('/', (req, res) => res.send('Hello World!'));

    router.use('/authentication',authenticationController(database));
    router.use('/books',booksController(database));
    router.use('/notes',notesController(database));
    router.use('/campaigns',campaignsController(database));
    router.use('/pageUpdates',pageUpdateController(database));

    router.use('/metadata',metadataController());

    app.use(router);

};