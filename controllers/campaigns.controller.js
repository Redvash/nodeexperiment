const express = require('express');

const authenticate = require('../middlewares/authenticate.middleware');
const metadata = require('../config/metadata.config');

module.exports = (database) => {
    //Register controller with metadata
    metadata.registerController({
        name: 'Campaigns',
        description: 'CRUD endpoints to manage user campaigns',
        url: '/campaigns',
    });

    let router = express.Router();

    router.get('', authenticate, (req,res) => {
        database.Campaign.findAll({})
            .then(campaigns => {
                res.send(campaigns);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.post('', authenticate, (req,res) => {
        let campaign = req.body;

        campaign.ownerId = req.user.id;

        let newCampaignInstance = database.Campaign.build(campaign);

        newCampaignInstance.save()
            .then(savedCampaignInstance => {
                res.send(savedCampaignInstance);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.put('/:campaignId', authenticate, (req,res) => {
        database.Campaign.findById(req.params.campaignId)
            .then(campaignInstance => {
                if(campaignInstance === null) {
                    res.status(404).send('No campaign found with such an Id');
                    return;
                }
                if(campaignInstance.ownerId !== req.user.id) {
                    res.status(403).send('You are not authorized to modify this campaign');
                    return;
                }

                let campaign = req.body;

                campaignInstance.update(campaign)
                    .then(updatedCampaignInstance => {
                        res.send(updatedCampaignInstance);
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.delete('/:campaignId', authenticate, (req,res) => {
        database.Campaign.findById(req.params.campaignId)
            .then(campaignInstance => {
                if(campaignInstance === null) {
                    res.status(404).send('No campaign found with such an Id');
                    return;
                }
                if(campaignInstance.ownerId !== req.user.id) {
                    res.status(403).send('You are not authorized to delete this campaign');
                    return;
                }

                campaignInstance.destroy()
                    .then(() => {
                        res.send(true);
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    return router;
};