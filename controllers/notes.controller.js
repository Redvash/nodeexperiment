const express = require('express');
const authenticate = require('../middlewares/authenticate.middleware');
const metadata = require('../config/metadata.config');

module.exports = (database) => {
    //Register controller with metadata
    metadata.registerController({
        name: 'Notes',
        description: 'CRUD endpoints to manage user notes',
        url: '/notes',
    });

    let router = express.Router();

    router.get('', authenticate, (req,res) => {
        if(!req.query.bookId) {
            res.status(400).send('Request missing required parameter: \'bookId\'');
            return;
        }

        database.Note.findAll({
            where: {
                bookId: req.query.bookId
            }
        })
            .then(notes => {
                res.send(notes);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.post('', authenticate, (req,res) => {
        let { bookId, note } = req.body;

        let newNoteInstance = database.Note.build({
            note,
            bookId
        });

        newNoteInstance.save()
            .then(savedNoteInstance => {
                res.send(savedNoteInstance);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.put('/:noteId', authenticate, (req,res) => {
        database.Note.findById(req.params.noteId)
            .then(noteInstance => {
                if(noteInstance === null) {
                    res.status(404).send('No note found with such an Id');
                    return;
                }

                let { note } = req.body;

                noteInstance.update({
                    note
                })
                    .then(updatedNoteInstance => {
                        res.send(updatedNoteInstance);
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.delete('/:noteId', authenticate, (req,res) => {
        database.Note.findById(req.params.noteId)
            .then(noteInstance => {
                if(noteInstance === null) {
                    res.status(404).send('No note found with such an Id');
                    return;
                }

                noteInstance.destroy()
                    .then(() => {
                        res.send(true);
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    return router;
};