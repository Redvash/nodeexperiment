const express = require('express');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const appConfig = require('../config/app.config');
const metadata = require('../config/metadata.config');

module.exports = (database) => {
    //Register controller with metadata
    metadata.registerController({
        name: 'Authentication',
        description: 'Endpoints to authenticate, logout and register users',
        url: '/authentication',
    });

    let router = express.Router();

    router.post('/authenticate', (req,res) => {
        let { userEmail, userPassword } = req.body;
        if(!userEmail || !userPassword) {
            res.status(400).send();
            return;
        }

        database.User.findOne({
            where: {
                email: userEmail.trim()
            }
        })
            .then(user => {
                if(user === null) {
                    res.status(400).send('Incorrect Email');
                    return;
                }
                if(user.password !== userPassword) {
                    res.status(400).send('Incorrect Password');
                    return;
                }

                let tokenPayload = {
                    userId: user.id,
                    createdDate: moment().toISOString()
                };
                let newAuthenticationToken = jwt.sign(tokenPayload,appConfig.secretKey);

                delete user.password;

                res.send({
                    user: user,
                    token: newAuthenticationToken
                });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.post('/register', (req,res) => {
        let { userEmail, userPassword } = req.body;
        if(!userEmail || !userPassword) {
            res.status(400).send();
            return;
        }

        database.User.findOne({
            where: {
                email: userEmail.trim()
            }
        })
            .then(user => {
                if(user !== null) {
                    res.status(400).send('Email already registered');
                    return;
                }

                database.User.build({
                    email: userEmail,
                    password: userPassword
                }).save()
                    .then(savedUserInstance => {
                        let tokenPayload = {
                            userId: savedUserInstance.id,
                            createdDate: moment().toISOString()
                        };
                        let newAuthenticationToken = jwt.sign(tokenPayload,appConfig.secretKey);

                        delete savedUserInstance.password;

                        res.send({
                            user: savedUserInstance,
                            token: newAuthenticationToken
                        });
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.post('/logout', (res,req) => {

    });

    return router;
};