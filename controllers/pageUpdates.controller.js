const express = require('express');
const authenticate = require('../middlewares/authenticate.middleware');
const metadata = require('../config/metadata.config');

module.exports = (database) => {
    //Register controller with metadata
    metadata.registerController({
        name: 'PageUpdates',
        description: 'Endpoints to get application update messages',
        url: '/pageUpdates',
    });

    let router = express.Router();

    router.get('', authenticate, (req,res) => {
        database.PageUpdate.findAll()
            .then(updates => {
                res.send(updates);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    return router;
};