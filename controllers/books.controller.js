const express = require('express');
const authenticate = require('../middlewares/authenticate.middleware');
const metadata = require('../config/metadata.config');


module.exports = (database) => {
    //Register controller with metadata
    metadata.registerController({
        name: 'Books',
        description: 'CRUD endpoints to manage user books',
        url: '/books',
    });

    let router = express.Router();

    router.get('', authenticate, (req,res) => {
        database.Book.findAll({
            where: {
                ownerId: req.user.id
            }
        })
            .then(books => {
                res.send(books);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.post('', authenticate, (req,res) => {
        let { name, category } = req.body;

        let newBookInstance = database.Book.build({
            name,
            category,
            ownerId: req.user.id
        });

        newBookInstance.save()
            .then(savedBookInstance => {
                res.send(savedBookInstance);
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.put('/:bookId', authenticate, (req,res) => {
        database.Book.findById(req.params.bookId)
            .then(bookInstance => {
                if(bookInstance === null) {
                    res.status(404).send('No book found with such an Id');
                    return;
                }
                if(bookInstance.ownerId !== req.user.id) {
                    res.status(403).send('User can\'t modify given book');
                    return;
                }

                let { name, category } = req.body;

                bookInstance.update({
                    name,
                    category
                })
                    .then(updatedBookInstance => {
                        res.send(updatedBookInstance);
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    router.delete('/:bookId', authenticate, (req,res) => {
        database.Book.findById(req.params.bookId)
            .then(bookInstance => {
                if(bookInstance === null) {
                    res.status(404).send('No book found with such an Id');
                    return;
                }
                if(bookInstance.ownerId !== req.user.id) {
                    res.status(403).send('User can\'t delete given book');
                    return;
                }

                bookInstance.destroy()
                    .then(() => {
                        res.send(true);
                    })
                    .catch(error => {
                        res.status(500).send(error);
                    });
            })
            .catch(error => {
                res.status(500).send(error);
            });
    });

    return router;
};