const express = require('express');
const bodyParser = require('body-parser');

const port = process.env.PORT || 8000;

const { registerMiddleware } = require('./middlewares');
const { registerControllers } = require('./controllers');

const database = require('./models');

const app = express();

app.use(bodyParser.json());

registerMiddleware(app);
registerControllers(app,database);

app.listen(port, () => console.log('Server listening on port ' + port + '!'));